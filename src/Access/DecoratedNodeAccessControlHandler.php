<?php

namespace Drupal\node_singles\Access;

use Drupal\Core\Entity\EntityAccessControlHandlerInterface;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Drupal\node\NodeAccessControlHandler;
use Drupal\node\NodeAccessControlHandlerInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Decorates the existing node access control handler.
 *
 * This prevents issues when multiple modules want to override the node access
 * control handler.
 */
class DecoratedNodeAccessControlHandler implements NodeAccessControlHandlerInterface, EntityAccessControlHandlerInterface, EntityHandlerInterface {

  public const STATE_KEY = 'node_singles.decorated_access_handler_class';

  /**
   * Information about the entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a DecoratedNodeAccessControlHandler object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   */
  public function __construct(
    EntityTypeInterface $entityType,
    EntityTypeManagerInterface $entityTypeManager,
    StateInterface $state
  ) {
    $this->entityType = $entityType;
    $this->entityTypeManager = $entityTypeManager;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $this->decorated()->access($entity, $operation, $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public function createAccess($entity_bundle = NULL, AccountInterface $account = NULL, array $context = [], $return_as_object = FALSE) {
    return $this->decorated()->createAccess($entity_bundle, $account, $context, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public function resetCache() {
    $this->decorated()->resetCache();
  }

  /**
   * {@inheritdoc}
   */
  public function setModuleHandler(ModuleHandlerInterface $module_handler) {
    $this->decorated()->setModuleHandler($module_handler);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account = NULL, FieldItemListInterface $items = NULL, $return_as_object = FALSE) {
    return $this->decorated()->fieldAccess($operation, $field_definition, $account, $items, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public function acquireGrants(NodeInterface $node) {
    return $this->decorated()->acquireGrants($node);
  }

  /**
   * {@inheritdoc}
   */
  public function writeDefaultGrant() {
    $this->decorated()->writeDefaultGrant();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteGrants() {
    $this->decorated()->deleteGrants();
  }

  /**
   * {@inheritdoc}
   */
  public function countGrants() {
    return $this->decorated()->countGrants();
  }

  /**
   * {@inheritdoc}
   */
  public function checkAllGrants(AccountInterface $account) {
    return $this->decorated()->checkAllGrants($account);
  }

  /**
   * Returns the decorated access control handler.
   *
   * @return \Drupal\node\NodeAccessControlHandlerInterface|\Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  private function decorated() {
    if (isset($this->decorated)) {
      return $this->decorated;
    }

    $originalAccessHandlerClass = $this->state->get(self::STATE_KEY, NodeAccessControlHandler::class);
    $handler = $this->entityTypeManager->createHandlerInstance($originalAccessHandlerClass, $this->entityType);

    if (!$handler instanceof NodeAccessControlHandlerInterface) {
      throw new \InvalidArgumentException('The decorated access control handler must implement NodeAccessControlHandlerInterface.');
    }

    if (!$handler instanceof EntityAccessControlHandlerInterface) {
      throw new \InvalidArgumentException('The decorated access control handler must implement EntityAccessControlHandlerInterface.');
    }

    return $handler;
  }

}
